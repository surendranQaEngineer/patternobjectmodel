package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLead;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHome;
import com.yalla.pages.MyLeads;
import com.yalla.pages.ViewLeadPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLeadAndViewLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Login into leaftaps";
		author = "Surendran";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void CreateLeadAndViewLead (String uName, String pwd, String CompName, String FName, String LName, String Email) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		//new HomePage()
		.clickcrmsfa()
		//new MyHome()
		.clickLeads()
		//new MyLeads()
		.clickCreateLead()
		//new CreateLead()
		.enterCompanyName(CompName)
		.enterFirstName(FName)
		.enterLastName(LName)
		.enterEmailAddress(Email)
		.submitLead()
		//new ViewLeadPage()
		.verifyFirstName();
		
		
	}
		
		
		
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	







