package com.yalla.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLead;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHome;
import com.yalla.pages.MyLeads;
import com.yalla.pages.ViewLeadPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_DuplicateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_DuplicateLead";
		testcaseDec = "Login into leaftaps";
		author = "Surendran";
		category = "smoke";
		excelFileName = "TC004";
	} 

	@Test(dataProvider="fetchData") 
	public void EditLead (String uName, String pwd, String Email) {
		
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickcrmsfa()
		.clickLeads()
		.ClickFindLeads()
		.clickEmailTab()
		.enterEmailAddress(Email)
		.ClickEmailFindLeadsButton()
		.ClickFirstEmail()
		.ClickDuplicateButton()
		.ClickCreateLeadButton();
	}
		
		
		
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	







