package com.yalla.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLead;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHome;
import com.yalla.pages.MyLeads;
import com.yalla.pages.ViewLeadPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Login into leaftaps";
		author = "Surendran";
		category = "smoke";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
	public void EditLead (String uName, String pwd, String CompName, String FirstName) {
		
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickcrmsfa()
		.clickLeads()
		.ClickFindLeads()
		.enterFirstName(FirstName)
		.clickFindLeadsButton()
		.ClickFirstResultingLead()
		.ClickEdit()
		.enterCompanyName(CompName)
		.ClickUpdateButton()
		.verifyCompanyName();
		
		
	}
		
		
		
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	







