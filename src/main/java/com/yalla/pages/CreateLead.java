package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class CreateLead extends Annotations{ 

	public CreateLead() {
       PageFactory.initElements(driver, this);
	} 
	
	@FindBy(how=How.ID, using="createLeadForm_companyName")  WebElement eleCompName;
	@FindBy(how=How.ID, using="createLeadForm_firstName")  WebElement eleFName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLName;
	@FindBy(how=How.XPATH, using="//input[@name='submitButton']") WebElement eleSubmit;
	@FindBy(how=How.ID, using="createLeadForm_primaryEmail") WebElement eleEmail;
	
	public CreateLead enterCompanyName(String CompName) {
		//WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleCompName, CompName);  
		return this; 
	}
	
	public CreateLead enterFirstName(String FName) {
		//WebElement elePassWord = locateElement("id", "password");
		clearAndType(eleFName, FName); 
		return this; 
	}
	
	public CreateLead enterLastName(String LName) {
		//WebElement elePassWord = locateElement("id", "password");
		clearAndType(eleLName, LName); 
		return this; 
	}
	
	public CreateLead enterEmailAddress(String Email) {
		clearAndType(eleEmail, Email);
		return this;
	}
	
	public ViewLeadPage submitLead() {
		click(eleSubmit);
		return new ViewLeadPage();
	}
		
	}









