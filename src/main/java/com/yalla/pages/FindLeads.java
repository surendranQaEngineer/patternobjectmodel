package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeads extends Annotations{ 

	public FindLeads() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleFirstName;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeadsButton;
	
	public FindLeads enterFirstName(String FirstName) {
			clearAndType(eleFirstName, FirstName);  
		return this;
	
}
	public FindLeads clickFindLeadsButton() {
          click(eleFindLeadsButton);  
          return this;
}
	@FindBy(how=How.XPATH, using="(//div/a[@class='linktext'])[4]") WebElement eleLeadList;
	public ViewLeadPage ClickFirstResultingLead() {
		click(eleLeadList);
		return new ViewLeadPage();
	}
	
	@FindBy(how=How.XPATH, using="//span//span[text()='Email']") WebElement eleEmailTab;
	@FindBy(how=How.XPATH, using="//input[@name='emailAddress']") WebElement eleEmail;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleEmailFindLeadsButton;
	public FindLeads clickEmailTab() {
		click(eleEmailTab);
		return this;
	}
	
	
	public FindLeads enterEmailAddress(String Email) {
		clearAndType(eleEmail, Email);
		return this;
	}
	
	public FindLeads ClickEmailFindLeadsButton() {
        click(eleEmailFindLeadsButton);  
        return this;
	
	}	
	
	@FindBy(how=How.XPATH, using="(//div/a[@class='linktext'])[4]") WebElement eleEmailList;
	public ViewLeadPage ClickFirstEmail() {
		click(eleEmailList);
		return new ViewLeadPage();
	}
	
	
	}

		
	









