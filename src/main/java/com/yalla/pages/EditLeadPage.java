package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class EditLeadPage extends Annotations{ 

	public EditLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	
	@FindBy(how=How.ID, using="updateLeadForm_companyName")  WebElement eleCompName;
	public EditLeadPage enterCompanyName(String CompName) {
		clearAndType(eleCompName, CompName);  
		return this; 
	}	
	@FindBy(how=How.XPATH, using="(//input[@class='smallSubmit'])[1]") WebElement eleUpdate;
	public ViewLeadPage ClickUpdateButton() {
		click(eleUpdate);
		return new ViewLeadPage();
	}
			
	}
	









