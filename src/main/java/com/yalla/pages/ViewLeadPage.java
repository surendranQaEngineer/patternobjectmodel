package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{ 

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.ID,using="viewLead_firstName_sp") WebElement eleFName;
	public ViewLeadPage verifyFirstName() {
		//WebElement eleFName = locateElement("id","viewLead_firstName_sp");
		verifyExactText(eleFName,"surendran");
		return new ViewLeadPage();
	}
	
	@FindBy(how=How.XPATH, using="//div/a[text()='Edit']") WebElement eleEdit;
	public EditLeadPage ClickEdit() {
		click(eleEdit);
		return new EditLeadPage();
	}
	@FindBy(how=How.ID, using="viewLead_companyName_sp") WebElement eleCName;
	public ViewLeadPage verifyCompanyName() {
		verifyExactText(eleCName,"JetRoadways");
		return new ViewLeadPage();
		
	}
	
	@FindBy(how=How.LINK_TEXT, using="Duplicate Lead") WebElement eleDuplicateButton;
	public DuplicateLeadPage ClickDuplicateButton() {
		click(eleDuplicateButton);
		return new DuplicateLeadPage();
	
}
}









