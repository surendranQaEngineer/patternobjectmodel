package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MyLeads extends Annotations{ 

	public MyLeads() {
       PageFactory.initElements(driver, this);
	} 
	
	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLead;
	public CreateLead clickCreateLead() throws InterruptedException {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateLead);
		Thread.sleep(5000);
		return new CreateLead();
	}
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement eleFindLeads;
	public FindLeads ClickFindLeads() {
		click(eleFindLeads);
		return new FindLeads();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Merge Leads") WebElement eleMergeLeads;
	public MergeLeads ClickMergeLeads() {
		click(eleMergeLeads);
		return new MergeLeads();
	}
}
		
	









