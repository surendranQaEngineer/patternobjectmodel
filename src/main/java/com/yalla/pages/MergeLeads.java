package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeads extends Annotations{ 

	public MergeLeads() {
       PageFactory.initElements(driver, this);
	} 
	
	@FindBy(how=How.XPATH, using="(//a/img[@alt='Lookup'])[1]") WebElement eleFLead;
	@FindBy(how=How.XPATH, using="(//div/a[@class='linktext'])[1]") WebElement eleFirstLead;
	public void ClickFromLead() {
		String parentWindow = driver.getWindowHandle();
		click(eleFLead);
		for(String childWindow : driver.getWindowHandles()){
		    driver.switchTo().window(childWindow).manage().window().maximize();
		    }
//		locateElement("xpath", "//div/input[@class=' x-form-text x-form-field ']").sendKeys("10458");
//		locateElement("xpath", "(//button[@type='button'])[1]").click();
//		locateElement("xpath", "(//div/a[@class='linktext'])[1]").click();
		
		driver.close();
		driver.switchTo().window(parentWindow);
		
		}
	@FindBy(how=How.XPATH, using= "//div/input[@class=' x-form-text x-form-field ']") WebElement eleLeadId;
	public MergeLeads enterLeadId(String LeadId) {
		clearAndType(eleLeadId, LeadId);
		return this;
	}
	@FindBy(how=How.XPATH, using="(//button[@type='button'])[1]") WebElement eleSubmit;
	public MergeLeads clickSubmit() {
		Click(edleSubmit);
	}
	@FindBy(how=How.XPATH, using="(//a/img[@alt='Lookup'])[2]") WebElement eleToLead;
	public void ClickToLead() {
		String parentWindow = driver.getWindowHandle();
		click(eleToLead);
		for(String childWindow : driver.getWindowHandles()){
		    driver.switchTo().window(childWindow).manage().window().maximize();
		    }
		locateElement("xpath", "//div/input[@class=' x-form-text x-form-field ']").sendKeys("10459");
		locateElement("xpath", "(//button[@type='button'])[1]").click();
		locateElement("xpath", "(//div/a[@class='linktext'])[1]").click();
		driver.close();
		driver.switchTo().window(parentWindow);
//	@FindBy(how=How.XPATH, using="//div/input[@class=' x-form-text x-form-field ']") WebElement eleLeadId;
//	public MergeLeads enterLeadId() {
//		clearandType(eleLeadId);
//		return this;
	
	}
	@FindBy(how=How.LINK_TEXT, using="Merge") WebElement eleMerge;
	public ViewLeadPage clickMerge() {
		click(eleMerge);
		driver.switchTo().alert().accept();
		return new ViewLeadPage();
	}
	}
	
	

		
	









