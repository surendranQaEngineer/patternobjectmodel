package steps;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {

	RemoteWebDriver driver;
	@Given("open the browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("maximize the browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@Given("load the url")
	public void loadTheUrl() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@Given("Enter the username as Demosalesmanager")
	public void enterTheUsernameAsDemosalesmanager() {
		driver.findElementById("username").sendKeys("DemoSalesManager");
	}

	@Given("Enter the password as crmsfa")
	public void enterThePasswordAsCrmsfa() {
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@When("Click on the login button")
	public void clickOnTheLoginButton() {
		driver.findElementByXPath("//input[@value='Login']").click();
	}

	@Then("verify login is successful")
	public void verifyLoginIsSuccessful() {
	    System.out.println("Login Successful");
	}

	@Then("Click on the crmsfa link")
	public void clickOnTheCRMSFALink() {
		driver.findElementByLinkText("CRM/SFA").click();
	}


	@Then("Click Create Lead Tab")
	public void clickCreateLeadTab() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Then("Enter the company name as google")
	public void enterTheCompanyNameAsGoogle() {
		driver.findElementByXPath("(//input[@name='companyName'])[2]").sendKeys("google");		
	}

	@Then("Enter the first name as suren")
	public void enterTheFirstNameAsSuren() {
		driver.findElementByXPath("//input[@id='createLeadForm_firstName']").sendKeys("suren", Keys.TAB);
	}

	@Then("Enter the last name as nageshwaran")
	public void enterTheLastNameAsNageshwaran() {
		driver.findElementByXPath("//input[@id='createLeadForm_lastName']").sendKeys("nageshwaran", Keys.TAB);
	}

	@When("Click on the create lead button")
	public void clickOnTheCreateLeadButton() {
		driver.findElementByXPath("//input[@name='submitButton']").click();
	}

	@Then("verify the new lead is created")
	public void verifyTheNewLeadIsCreated() {
	    System.out.println("New lead successfully created");
	}
}




